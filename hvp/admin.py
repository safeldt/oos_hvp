from django.contrib import admin
from hvp.models import *

# Register your models here.

admin.site.register(Household)
admin.site.register(Entry)
admin.site.register(UserProfile)
