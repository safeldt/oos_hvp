#!C:\Python36
# -*- encoding: utf-8 -*-

__author__ = 'safeldt'

from django.forms import *
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from hvp.models import *


class HouseholdForm(ModelForm):
    """
    A form to create a household.
    """
    class Meta:
        model = Household
        labels = {
            'name': 'Name',
            'street': 'Straße',
            'street_no': 'Nr.',
            'zip': 'PLZ',
            'place': 'Ort'
        }
        exclude = {'owner', 'guests'}


class EntryForm(ModelForm):
    """
    A from to enter meter reading entries.
    """
    class Meta:
        model = Entry
        labels = {
            'meter_reading': 'Zählerstand',
            'is_calculated': 'Berechnet',
            'date': 'Datum'
        }
        # fields = '__all__'
        exclude = {'household'}
        widgets = {
            'date': DateInput(attrs={'type': 'date'})
        }


class RegistrationForm(UserCreationForm):
    """
    User registration form.
    """
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = {
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        }
        fields_order = {'first_name', 'last_name', 'username', 'email', 'password1', 'password2'}

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class EditProfileForm(UserChangeForm):
    """
    Edit profile form for users.
    """
    class Meta:
        model = User
        fields = {
            'username',
            'email',
            'first_name',
            'last_name',
            'password'
        }
