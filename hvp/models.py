from django.db import models
from datetime import date
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db.models.signals import post_save


class Household(models.Model):
    """
    Represents a household.
    """
    name = models.CharField(max_length=50, default='')
    street = models.CharField(max_length=50, default='')
    street_no = models.CharField(max_length=50, default='')
    zip = models.CharField(
        max_length=5,
        validators=[
            RegexValidator(
                regex='^\d{5}$',
                message='Die PLZ muss aus 5 Ziffern bestehen',
                code='nomatch'
            )
        ],
        default=''
    )
    place = models.CharField(max_length=50, default='')
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    guests = models.ManyToManyField(User, related_name='guests')

    def __str__(self):
        return self.name


class Entry(models.Model):
    """
    Represents a meter reading entry.
    """
    meter_reading = models.DecimalField(max_digits=8, decimal_places=3)
    is_calculated = models.BooleanField(default=False)
    date = models.DateField(default=date.today)
    household = models.ForeignKey(Household, on_delete=models.CASCADE)

    def __str__(self):
        return "{}, {}, {}, {}".format(self.date, self.meter_reading, self.is_calculated, self.household)


class UserProfile(models.Model):
    """
    Represents a user.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    activation_key = models.CharField(max_length=255, default=1)
    email_validated = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])


post_save.connect(create_profile, sender=User)
