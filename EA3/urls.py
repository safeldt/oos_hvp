"""EA3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.auth.views import (
    login,
    logout,
    password_reset,
    password_reset_done,
    password_reset_confirm,
    password_reset_complete
)
from hvp import views

urlpatterns = [
    # admin interface
    path('admin/', admin.site.urls),

    # home page
    path('', views.index, name='index'),

    # user management
    path('accounts/register/', views.register, name='register'),
    path('accounts/login/', login, {'template_name': 'accounts/login.html'}, name='login'),
    path('accounts/logout/', logout, {'template_name': 'accounts/logout.html'}, name='logout'),
    path('accounts/profile/', views.view_profile, name='viewProfile'),
    path('accounts/profile/edit/', views.edit_profile, name='editProfile'),
    path('accounts/profile/edit/change-password/', views.change_password, name='changePassword'),
    path(
        'accounts/reset-password/',
        password_reset,
        {'template_name': 'accounts/forgot_password.html'},
        name='password_reset'
    ),
    path('accounts/reset-password/done/', password_reset_done, name='password_reset_done'),
    re_path(
        r'accounts/reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm,
        name='password_reset_confirm'
    ),
    path('accounts/reset-password/complete/', password_reset_complete, name='password_reset_complete'),

    # show/add/edit households
    path('households/', views.households_list, name='householdList'),
    path('households/add/', views.household, name='addHousehold'),
    path('households/edit/<int:household_id>/', views.household, name='editHousehold'),
    path('households/details/<int:household_id>/', views.household_detail, name='detailHousehold'),
    path('households/delete/<int:household_id>/', views.delete_household, name='deleteHousehold'),

    # show/add/edit meter readings
    path('households/<int:household_id>/entries/', views.entries_list, name='entriesList'),
    path('households/<int:household_id>/entries/add-many/', views.create_many_entries, name='addManyEntries'),
    path('households/<int:household_id>/entries/add/', views.entry, name='addEntry'),
    path('households/<int:household_id>/entries/edit/<int:entry_id>/', views.entry, name='editEntry'),
    path('households/<int:household_id>/entries/delete/<int:entry_id>/', views.delete_entry, name='deleteEntry'),
]
