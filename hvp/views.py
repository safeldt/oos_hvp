from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
from django.db.models import Q
from hvp.forms import *
from hvp.models import Household, Entry
from random import randint
import datetime


@login_required
def create_many_entries(request, household_id):
    """
    This is a dev function to create instantly many meter readings.
    Could be used in future to fill gaps in meter reading entries automatically.
    :param request:
    :param household_id:
    :return:
    """
    if Household.objects.filter(
            Q(owner=request.user, id=household_id) | Q(guests=request.user, id=household_id)).exists():
        sd = date.today() - datetime.timedelta(days=100)
        smr = randint(0, 300)

        while sd < date.today():
            e = Entry()
            e.household = Household.objects.get(pk=household_id)
            e.meter_reading = smr
            e.date = sd
            e.save()
            sd += datetime.timedelta(days=1)
            smr += randint(0, 9)

        messages.success(request, u'Zählerstände wurden generiert.')
    else:
        messages.error(request, u'Zählerstände wurden nicht generiert. Möglicherweise fehlen die Berechtigungen')

    args = {
        'page_title': 'Einträge',
        'household_id': household_id
    }
    return redirect('entriesList', household_id)


@login_required
def delete_entry(request, household_id, entry_id):
    """
    View to delete a meter reading entry.
    :param request:
    :param household_id:
    :param entry_id:
    :return:
    """
    if Household.objects.filter(
            Q(owner=request.user, id=household_id) | Q(guests=request.user, id=household_id)).exists():
        Entry.objects.get(pk=entry_id).delete()
        messages.success(request, u'Zählerstand wurde gelöscht.')
    else:
        messages.error(request, u'Zählerstand wurde nicht gelöscht. Möglicherweise fehlen die Berechtigungen')
    return redirect('entriesList', household_id)


@login_required
def entry(request, household_id, entry_id=None):
    """
    View to create or edit a meter reading entry.
    :param request:
    :param household_id:
    :param entry_id:
    :return:
    """
    if entry_id is None and Household.objects.filter(
            Q(owner=request.user, id=household_id) | Q(guests=request.user, id=household_id)).exists():
        # 'mr' means 'meter reading'
        mr = Entry()

        # check if meter reading has already been recorded today
        if not Entry.objects.filter(date__gte=date.today(), household=household_id):
            mr.household_id = household_id
        else:
            messages.error(request, u'Der Zählerstand wurde heute bereits erfasst.')

        page_title = 'Zählerstand hinzufügen'
    else:
        mr = get_object_or_404(Entry, pk=entry_id)
        page_title = 'Zählerstand bearbeiten'

    if request.method == 'POST':
        form = EntryForm(request.POST, instance=mr)

        # check if new meter reading is greater then all of this household
        if form.is_valid() and not Entry.objects.filter(meter_reading__gt=request.POST.get('meter_reading'), household=household_id):
            form.save()
            messages.success(request, u'Zählerstand erfolgreich gespeichert.')
            return redirect('entriesList', household_id)
        else:
            messages.error(request, u'Eingabe war fehlerhaft.')
            pass
    else:
        form = EntryForm(instance=mr)

    args = {
        'page_title': page_title,
        'form': form,
        'household_id': household_id
    }
    return render(request, 'entry.html', args)


@login_required
def entries_list(request, household_id):
    """
    View for list of entries of a given household.
    :param request:
    :param household_id:
    :return:
    """
    entries = Entry.objects.filter(
        Q(household__owner=request.user, household=household_id) |
        Q(household__guests__username=request.user, household=household_id)
    ).order_by('-meter_reading')

    args = {
        'page_title': 'Einträge',
        'entries': entries,
        'hh_id': household_id
    }
    return render(request, 'entry_list.html', args)


@login_required
def household(request, household_id=None):
    """
    View to create or edit a household.
    :param request:
    :param household_id:
    :return:
    """
    if household_id is None:
        hh = Household()
        hh.owner = request.user
        page_title = 'Haushalt hinzufügen'
        button_text = 'Haushalt erstellen'
    else:
        hh = get_object_or_404(Household, pk=household_id, owner=request.user)
        page_title = 'Haushalt bearbeiten'
        button_text = 'Änderung speichern'

    if request.method == 'POST':
        form = HouseholdForm(request.POST, instance=hh)
        if form.is_valid():
            form.save()
            messages.success(request, u'Haushalt erfolgreich gespeichert.')
            return redirect('householdList')
        else:
            messages.error(request, u'Eingabe war fehlerhaft.')
            pass
    else:
        form = HouseholdForm(instance=hh)

    args = {
        'page_title': page_title,
        'form': form,
        'button_text': button_text
    }
    return render(request, 'household.html', args)


@login_required
def delete_household(request, household_id):
    """
    Function to delete a household.
    :param request:
    :param household_id:
    :return:
    """
    if Household.objects.filter(owner=request.user, id=household_id):
        Household.objects.get(pk=household_id).delete()
        messages.success(request, u'Haushalt wurde gelöscht.')
    else:
        messages.error(request, u'Haushalt wurde nicht gelöscht. Möglicherweise fehlen die Berechtigungen')
    return redirect('householdList')


@login_required
def household_detail(request, household_id):
    """
    Detail view of a household with some calculations like average consumption.
    :param request:
    :param household_id:
    :return:
    """
    latest_mr = Entry.objects.filter(household_id=household_id).latest('date')
    date_30_days_ago = latest_mr.date - datetime.timedelta(days=30)
    entry_30_days_ago = Entry.objects.filter(household_id=household_id, date=date_30_days_ago).first()
    entries_last_30_days = Entry.objects.filter(household_id=household_id, date__gt=date_30_days_ago).count()

    date_7_days_ago = latest_mr.date - datetime.timedelta(days=7)
    entry_7_days_ago = Entry.objects.filter(household_id=household_id, date=date_7_days_ago).first()
    entries_last_7_days = Entry.objects.filter(household_id=household_id, date__gt=date_7_days_ago).count()

    avg_consumption_30 = (latest_mr.meter_reading - entry_30_days_ago.meter_reading) / 30
    avg_consumption_7 = (latest_mr.meter_reading - entry_7_days_ago.meter_reading) / 7

    args = {
        'page_title': 'Haushalt im Überblick',
        'hh_id': household_id,
        'latest_mr': latest_mr,
        'entries_last_30_days': entries_last_30_days,
        'entry_30_days_ago': entry_30_days_ago,
        'entries_last_7_days': entries_last_7_days,
        'entry_7_days_ago': entry_7_days_ago,
        'avg_consumption_30': avg_consumption_30,
        'avg_consumption_7': avg_consumption_7,
    }
    return render(request, 'household_detail.html', args)


@login_required
def households_list(request):
    """
    List view of all households a user has created or has been invited to.
    :param request:
    :return:
    """
    hh = Household.objects.filter(owner=request.user).order_by('name')
    hh_guest = Household.objects.filter(guests=request.user).order_by('name')
    args = {
        'page_title': 'Haushalte',
        'hh': hh,
        'hh_guest': hh_guest,
    }
    return render(request, 'household_list.html', args)


def index(request):
    """
    Simple index view.
    :param request:
    :return:
    """
    args = {
        'page_title': 'Willkommen'
    }
    return render(request, 'index.html', args)


def register(request):
    """
    Registration form view.
    :param request:
    :return:
    """
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegistrationForm()
        args = {
            'page_title': 'Registrieren',
            'form': form
        }
        return render(request, 'accounts/register.html', args)


@login_required
def view_profile(request):
    """
    View user profile.
    :param request:
    :return:
    """
    args = {
        'user': request.user,
        'page_title': 'Profil'
    }
    return render(request, 'accounts/profile.html', args)


@login_required
def edit_profile(request):
    """
    View to edit user profile.
    :param request:
    :return:
    """
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, u'Profil erfolgreich gespeichert.')
            return redirect('viewProfile')
        else:
            messages.error(request, u'Eingabe war fehlerhaft.')
            pass
    else:
        form = EditProfileForm(instance=request.user)
        args = {
            'page_title': 'Profil bearbeiten',
            'form': form,
        }
        return render(request, 'accounts/edit_profile.html', args)


@login_required
def change_password(request):
    """
    Change password view.
    :param request:
    :return:
    """
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, u'Passwort erfolgreich geändert.')
            return redirect('changePassword')
        else:
            messages.error(request, u'Eingabe war fehlerhaft.')
            pass
    else:
        form = PasswordChangeForm(user=request.user)
        args = {
            'page_title': 'Passwort ändern',
            'form': form,
        }
        return render(request, 'accounts/change_password.html', args)
